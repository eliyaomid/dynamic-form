(function ( $ ) {

	var currentTab = 0;

	$.fn.buildForm = function(formObject) {
		formObject = formObject.form;
		makeFormTitle(this, formObject);
		var steps = formObject.steps;
		makestepElements(this, steps);
		makeFormStepIndicator(this, formObject);
		runForm(this);
		return this;
	};
	function runForm() {
		showTab(currentTab);
	}
	function showTab(n) {
		var x = $(".step").removeClass("active");
		$(x[n]).addClass("active");
	}
	function makeFormTitle(formElement, formObject) {
		var formTitle = formObject.title;
		var formSubTitle = formObject.subtitle;
		formElement.append('<h1 class="title">' + formTitle + '</h1>');
		if (formSubTitle) {
			formElement.append('<h2 class="sub-title">' + formSubTitle + '</h2>');
		}
	}
	function makeFormStepIndicator(formElement, formObject) {
		var stepIndicator = $('<div class="step-indicator"></div>');
		for (index in formObject.steps) {
			index = index - 1;
			var spanElement = $('<span class="istep"></span>');
			spanElement.on("click", { n: index }, function(event) { showTab(event.data.n);});
			stepIndicator.append(spanElement);
			
		}
		formElement.append(stepIndicator);
	}
	function makestepElements(formElement, steps) {
		$.each(steps, function( stepIndex, step ) {
			var stepElement = $('<div class="step step-' + stepIndex + '"></div>');
			var stepTitle = step.title;
			var stepSubTitle = step.subtitle;
			stepElement.append('<h3 class="title">' + stepTitle + '</h3>');
			if (stepSubTitle) {
				stepElement.append('<h4 class="sub-title">' + stepSubTitle + '</h4>');
			}
			var sections = step.section;
			makesectionElements(stepElement, sections);
			formElement.append(stepElement);
		});
	}
	function makesectionElements(stepElement, sections) {
		$.each(sections, function( sectionIndex, section ) {
			if (section.questions == undefined) {
				var sectionElement = $('<div class="section section-' + sectionIndex + '"></div>');
				makesectionElements(sectionElement, section);
			}
			else {
				var sectionElement = $('<div class="section section-' + sectionIndex + '"></div>');
				var sectionTitle = section.title;
				var sectionSubTitle = section.subtitle;
				sectionElement.append('<h5 class="title">' + sectionTitle + '</h5>');
				if (sectionSubTitle) {
					sectionElement.append('<h6 class="sub-title">' + sectionSubTitle + '</h6>');
				}
				makequestionsElements(sectionElement, section.questions);
			}
			stepElement.append(sectionElement);
		});
	}
	function makequestionsElements(sectionElement, questions) {
		$.each(questions, function( questionIndex, question ) {
			var questionTitle = question.title;
			var questionType = question.type;
			var questionValidation = question.validation;
			var questionElement = null;
			switch ( questionType ) {
				case "textbox":
					questionElement = '<p><input type="text"></p>';
					break;
			    case "combobox":
			    	var responses = question.responses;
			    	questionElement = '<select>';
			    	for (let response of responses) {
		    			questionElement += '<option value="' + response + '">' + response + '</option>';
			    	}
			    	questionElement += '</select>';
					break;
			    case "checkbox":
			    	var responses = question.responses;
			    	checkboxElement = "";
			    	for (let response of responses) {
		    			checkboxElement += '<p><input type="checkbox">' + response + '</p>';
			    	}
			    	if (checkboxElement) {
			    		questionElement = checkboxElement;
			    	}
					break;
				case "multiline":
					questionElement = '<p><textarea></textarea></p>';
					break;
			}
			if (questionElement) {
				sectionElement.append('<p>' + questionTitle + '</p>');
				sectionElement.append(questionElement);
			}
		});
	}
}( jQuery ));

$(document).ready(function() {
	var formObject = { "form": { "title": "یک فرم خوب", "subtitle": "اینجا توضیحات در صورت لزوم میاد", "steps": { "1": { "title": "تیتر ویزارد اول", "subtitle": "اگر توضیحی برای صفحه اول باشه", "section": { "1": { "title": "تیتر بخش", "subtitle": "توضیحات بخش", "questions": { "1": { "title": "پرسش شماره یک", "type": "textbox", "validation": "int", "responses": [] }, "2": { "title": "پرسش شماره دو", "type": "combobox", "validation": "NULL", "responses": [ "گزینه یک", "گزینه دوم" ] } } } } }, "2": { "title": "تیتر ویزارد دوم", "subtitle": "اگر توضیحی برای صفحه دوم باشه", "section": { "1": { "1": { "title": "تیتر بخش", "subtitle": "توضیحات بخش", "questions": { "1": { "title": "پرسش شماره یک", "type": "multiline", "validation": "NULL", "responses": [] }, "2": { "title": "پرسش شماره دو", "type": "combobox", "validation": "NULL", "responses": [ "گزینه یک", "گزینه دوم" ] } } }, "2": { "title": "تیتر بخش", "subtitle": "توضیحات بخش", "questions": { "1": { "title": "پرسش شماره یک", "type": "checkbox", "validation": "NULL", "responses": [ "15-18", "20-40", "بیشتر" ] } } } } } } } } };

	$("#main-form").buildForm(formObject);
});
